const chai = require('chai');
const { assert } = require('chai');

const http = require('chai-http');
chai.use(http);


describe("api_test_suite_users", ()=>{

    it("test_api_get_users_is_running", (done)=>{

        chai.request("http://localhost:4000")
        .get("/users")
        .end((err, res)=>{
            assert.isDefined(res);
            done();
        })
    })

    it("test_api_get_users_returns_array", (done)=>{

        chai.request("http://localhost:4000")
        .get("/users")
        .end((err, res) => {
            //console.log(res.body);
            assert.isArray(res.body);
            done();
        })
    })

    it("test_api_get_users_array_first_object_name_is_John", (done)=>{

        chai.request("http://localhost:4000")
        .get("/users")
        .end((err, res) => {
            //assert.equal(res.body[0],"John");
            assert.equal(res.body[0].name,"John");
            done();
        })
    })

    it("test_api_get_users_array_last_item_is_not_undefined", (done)=>{

        chai.request("http://localhost:4000")
        .get("/users")
        .end((err, res) => {
            //assert.equal(res.body[res.body.length-1],"Smith");
            assert.notEqual(res.body[res.body.length-1], undefined);
            done();
        })
    })

    it("test_api_post_users_returns_400_if_no_name", (done)=>{

        chai.request("http://localhost:4000")
        .post("/users")
        .type("json")
        .send({
            age: 31,
            username: "irene91"
        })
        .end((err, res)=>{
            assert.equal(res.status, 400);
            done();
        })
    })

    /* ACTIVITY */

    it("test_api_post_users_is_running", (done)=>{

        chai.request("http://localhost:4000")
        .post("/users")
        .end((err, res)=>{
            assert.isDefined(res);
            done();
        })
    })

    it("test_api_post_users_returns_400_if_no_username", (done)=>{

        chai.request("http://localhost:4000")
        .post("/users")
        .type("json")
        .send({
            age: 31,
            name: "irene"
        })
        .end((err, res)=>{
            assert.equal(res.status, 400);
            done();
        })
    })

    it("test_api_post_users_returns_400_if_no_age", (done)=>{

        chai.request("http://localhost:4000")
        .post("/users")
        .type("json")
        .send({
            name: "irene",
            username: "irene91"
        })
        .end((err, res)=>{
            assert.equal(res.status, 400);
            done();
        })
    })

});


describe("api_test_suite_products", ()=>{

    it("test_api_get_products_is_not_undefined", (done)=>{

        chai.request("http://localhost:4000")
        .get("/products")
        .end((err, res)=>{
            assert.isDefined(res);
            done();
        })
    })

    it("test_api_get_products_returns_array", (done)=>{

        chai.request("http://localhost:4000")
        .get("/products")
        .end((err, res) => {
            //console.log(typeof(res.body));
            assert.isArray(res.body);;
            done();
        })
    })

    it("test_api_get_products_array_first_item_is_object", (done)=>{

        chai.request("http://localhost:4000")
        .get("/products")
        .end((err, res) => {
            //assert.equal(typeof(res.body[0]),'object')
            assert.isObject(res.body[0]);
            done();
        })
    })

});




/*
    chai-http   - Import and use chai-http to allow to send requests to out server

    describe()  - test suite
    it()        - test case
    done()      - method used to tell chai http when the test is done
    res.body    - contains the body of the response, data sent fro
    .request()  - method is used from chai to create an http request to the given server
    .type()     - used to tell chai that request body is going to be stringified as JSON
    .send()     - used to send request body


    .get("/endpoint")   - used to access the get response from the route
                        - has 2 anon functions as an argument that receives 2 obj, err and response obj
    .post("/endpoint")  - used by chai http to access a post method route


    .isDefined      - an assertion to check if given data is not undefined
                    - similar to .notEqual(typeof(data),undefined)
    .isArray()      - an assertion that the given data is an array
    .isObject()     - an assertion that the given data is an object

*/