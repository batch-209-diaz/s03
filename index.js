// index.js


const express = require("express");

const app = express();
app.use(express.json());

const port = 4000;


let users_1 = ["John", "Johnson", "Smith"];

let users = [

    {
        name: "John",
        age: 18,
        username: "johnsmith99"
    },
    {
        name: "Johnson",
        age: 21,
        username: "johnson1991"
    },
    {
        name: "Smith",
        age: 19,
        username: "smithMike12"
    }
];

let products = [

    {
        name:"cake",
        price: 500,
        isActive: true
    },
    {
        name:"pastry",
        price: 55,
        isActive: true
    },
    {
        name:"tea",
        price: 78,
        isActive: true
    }
];

app.get('/users', (req, res)=>{
    return res.send(users);
});

app.post('/users', (req, res)=>{

    if(!req.body.hasOwnProperty("name")){
        return res.status(400).send({
            error: "Bad Request - missing required parameter NAME"
        });
    }

    if(!req.body.hasOwnProperty("age")){
        return res.status(400).send({
            error: "Bad Request - missing required parameter AGE"
        });
    }

    if(!req.body.hasOwnProperty("username")){
        return res.status(400).send({
            error: "Bad Request - missing required parameter USERNAME"
        });
    }
});

app.get('/products', (req, res)=>{
    return res.send(products);
});



app.listen(port,()=>console.log("ExpressJS API running at localhost:4000"))

